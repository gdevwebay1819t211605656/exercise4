import { Container , Graphics, Text } from "pixi.js";
import { create } from "domain";
import KeyCodes from "../KeyCodes";
import KeyValues from "../KeyValues";


class TitleScreen extends Container{
    constructor(){
        super();

        this.interactive = true;

        this.bgGraphics = new Graphics();
        this.bgGraphics.beginFill(0xFF0000);
        this.bgGraphics.drawRect(100,100,100,100);
        this.bgGraphics.endFill();
        this.addChild(this.bgGraphics);

        this.displayText = new Text(" ",{
            fill : [0xFF0000],
            fontSize : 20,
        });
        this.displayText.x = 400;
        this.displayText.y = 100;
        this.addChild(this.displayText);
        


        this.bgmInstance = createjs.Sound.play("assets/audio/Price.mp3",{
            loop: 1,
            volume: 1.0,
            delay: 100,
        });
        

        window.addEventListener("keydown", this.onKeyDown.bind(this));


    }

    mousedown(e)
    {
        if(this.bgGraphics.getBounds().contains(e.data.global.x, e.data.global.y))
        {
            this.bgmInstance.paused = !this.bgmInstance.paused;
        }
    }

    onKeyDown(e)
    {
        if(KeyCodes.KeyArrowUp == e.keyCode)
        {
            this.bgGraphics.y -= 1;
        }
        if(KeyCodes.KeyArrowDown == e.keyCode)
        {
            this.bgGraphics.y += 1;
        }

        if(KeyCodes.KeyArrowLeft == e.keyCode)
        {
            this.bgGraphics.x -= 1;
        }
        if(KeyCodes.KeyArrowRight == e.keyCode)
        {
            this.bgGraphics.x += 1;
        }

        if(KeyCodes.Key1 == e.keyCode)
        {
            this.sfxInstance1 = createjs.Sound.play("assets/audio/Persona 5 Notification Tone.mp3",{
                volume: 1.0,
                delay: 100,
            })
            //this.displayText.text += KeyValues[e.keyCode];
        }

        if(KeyCodes.Key2 == e.keyCode)
        {
            this.sfxInstance2 = createjs.Sound.play("assets/audio/Persona Summon Sound Effect.mp3",{
                volume: 1.0,
                delay: 100,
            })
            //this.displayText.text += KeyValues[e.keyCode];
        }

        if(KeyCodes.Key3 == e.keyCode)
        {
            this.sfxInstance3 = createjs.Sound.play("assets/audio/Persona 5 Stat Up.mp3",{
                volume: 1.0,
                delay: 100,
            })
            //this.displayText.text += KeyValues[e.keyCode];
        }

        if(KeyCodes.KeyA == e.keyCode)
        {
            this.bgmInstance.paused = !this.bgmInstance.paused;
            //this.displayText.text += KeyValues[e.keyCode];
        }

        if(KeyCodes.KeyBackspace == e.keyCode)
        {
            this.displayText.text = this.displayText.text.substring(0, this.displayText.text.length - 1);
        }

        else
        {
            
            this.displayText.text += KeyValues[e.keyCode];
        }

        
    }
   
}

export default TitleScreen;